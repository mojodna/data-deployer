#!/usr/bin/env node

"use strict";

var exec = require("child_process").exec,
    url = require("url"),
    util = require("util");

var async = require("async");

var dd = require("./lib");

var manifest = require(process.cwd() + "/manifest.json"),
    prev = {};

try {
  prev = require(process.cwd() + "/manifest_prev.json");
} catch (e) {
}

var actions = dd.getActions(prev, manifest);

var drivers = {};

var env = process.env;

var db = url.parse(process.env.DATABASE_URL);

if (db.auth) {
  var creds = db.auth.split(":", 2);

  env.PGUSER = creds[0];
  env.PGPASSWORD = creds[1] || "";
}

if (db.port) {
  env.PGPORT = db.port;
}

env.PGDATABASE = db.path.slice(1);
env.PGHOST = db.hostname;

// console.log(env);

// TODO (generally) check for injection errors

drivers.ogr = function(options, callback) {
  // console.log("ogr args:", options);

  var commands = [];

  switch (true) {
  case options.action === "create":
  case options.action === "update":
    console.log("Creating/updating '%s'", options.layer);
    commands.push(util.format("ogr2ogr " +
                              "--config PG_USE_COPY YES " +
                              "-t_srs EPSG:900913 " +
                              "-nln %s " +
                              "-nlt PROMOTE_TO_MULTI " +
                              "-lco POSTGIS_VERSION=2.0 " +
                              "-lco DROP_TABLE=IF_EXISTS " +
                              "-lco GEOMETRY_NAME=geom " +
                              "-lco SRID=900913 " +
                              "-f PGDump " +
                              "/vsistdout/ " +
                              "/vsizip/vsicurl/%s " +
                              "| psql -q",
                              options.layer,
                              options.source));
    break;

  case options.action === "remove":
    console.log("Removing '%s'", options.layer);
    commands.push(util.format("psql -c \"DROP TABLE %s\"", options.layer));
    break;
  }

  async.eachSeries(commands, function(cmd, done) {
    console.log(">>", cmd);
    // TODO spawn so output can be piped, return codes checked, and injection can be mitigated
    exec(cmd, {
      env: env
    }, function(err, stdout, stderr) {
      process.stdout.write(stdout);
      process.stderr.write(stderr);

      return done(err);
    });
  }, function(err) {
    console.log("%s updated to %s", options.layer, options.version);

    return callback(err);
  });
};

drivers.psql = function(options, callback) {
  // console.log("psql args:", options);

  var commands = [];

  switch (true) {
  // not something that's directly mutable
  case !!options.sql:
    commands.push(util.format("psql -q -c \"%s\"", options.sql));
    break;

  case options.action === "remove":
    console.log("Removing '%s'", options.layer);
    commands.push(util.format("psql -c \"DROP TABLE %s\"", options.layer));
    break;

  // create / update; assume that the sql file does the right thing
  case !!options.source:
    console.log("Creating/updating '%s'", options.layer);
    var source = url.parse(options.source);

    switch (source.protocol) {
    case "file:":
      // TODO check extension for necessary decompression

      commands.push(util.format("psql -q -f \".%s\"", source.path));
      break;

    default:
      return callback(new Error("Unsupported source protocol: " + source.protocol));
    }
  }

  async.eachSeries(commands, function(cmd, done) {
    console.log(">>", cmd);
    // TODO spawn so output can be piped, return codes checked, and injection can be mitigated
    exec(cmd, {
      env: env
    }, function(err, stdout, stderr) {
      process.stdout.write(stdout);
      process.stderr.write(stderr);

      return done(err);
    });
  }, function(err) {
    console.log("%s updated to %s", options.layer, options.version);

    return callback(err);
  });
};

async.eachSeries(actions, function(x, done) {
  if (typeof(drivers[x.driver]) === "function") {
    return drivers[x.driver](x, done);
  } else {
    console.warn("Unrecognized driver in:", x);

    return done();
  }
}, function(err) {
  if (err) {
    console.error(err);

    throw err;
  }

  console.log("Done.");
});
