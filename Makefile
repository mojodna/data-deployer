first:
	dropdb data
	createdb data
	cp initial_manifest.json manifest.json
	foreman run node index.js

next:
	cp initial_manifest.json manifest_prev.json
	cp next_manifest.json manifest.json
	foreman run node index.js
