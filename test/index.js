"use strict";

var assert = require("assert");
var jsonDiff = require("jsondiffpatch").diff;
var dd = require("../lib");

describe("#findChanges", function() {
  it("returns the path and diff with one change", function() {
    var a = {
      c: {
        d: "x"
      }
    };

    var b = {
      c: {
        d: "y"
      }
    };

    var diff = jsonDiff(a, b);

    var changes = dd.findChanges(diff);

    assert.deepEqual(["x", "y"], changes[0].diff);
    assert.deepEqual(["c", "d"], changes[0].path);
  });

  it("returns the path and diff with multiple changes", function() {
    var a = {
      c: {
        d: "x"
      },
      f: 1
    };

    var b = {
      c: {
        d: "y"
      },
      f: 2
    };

    var diff = jsonDiff(a, b);

    var changes = dd.findChanges(diff);

    assert.deepEqual(["x", "y"], changes[0].diff);
    assert.deepEqual(["c", "d"], changes[0].path);

    assert.deepEqual([1, 2], changes[1].diff);
    assert.deepEqual(["f"], changes[1].path);
  });

  it("returns the path and diff with changes, removals, and additions", function() {
    var a = {
      c: {
        d: "x"
      },
      f: 1
    };

    var b = {
      c: {
        d: "y"
      },
      g: 2
    };

    var diff = jsonDiff(a, b);

    var changes = dd.findChanges(diff);

    assert.deepEqual(["x", "y"], changes[0].diff);
    assert.deepEqual(["c", "d"], changes[0].path);

    assert.deepEqual([2], changes[1].diff);
    assert.deepEqual(["g"], changes[1].path);

    assert.deepEqual([1, 0, 0], changes[2].diff);
    assert.deepEqual(["f"], changes[2].path);
  });
});

describe("#getActions", function() {
  describe("on the first run", function() {
    describe("with a simple Shapefile layer", function() {
      before(function() {
        this.source = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp";

        this.manifest = {
          layers: {
            locations: this.source
          }
        };

        this.actions = dd.getActions(this.manifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.source, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("locations", this.actions[0].layer);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });

      it("should set 'version'", function() {
        assert.ok(this.actions[0].version);
      });
    });

    describe("with a simple versioned Shapefile layer", function() {
      before(function() {
        this.source = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp?version=1";

        this.manifest = {
          layers: {
            locations: this.source
          }
        };

        this.actions = dd.getActions(this.manifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should set the source properly", function() {
        assert.equal(this.source, this.actions[0].source);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });
    });

    describe("with an explicit Shapefile/OGR layer", function() {
      before(function() {
        this.source = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp";

        this.manifest = {
          layers: {
            locations: {
              driver: "ogr",
              source: this.source
            }
          }
        };

        this.actions = dd.getActions(this.manifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should set the source properly", function() {
        assert.equal(this.source, this.actions[0].source);
      });

      it("should set 'layer' properly", function() {
        assert.equal("locations", this.actions[0].layer);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });
    });

    describe("with an explicit Shapefile source", function() {
      before(function() {
        this.manifest = {
          layers: {
            locations: {
              source: "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp"
            }
          }
        };

        this.actions = dd.getActions(this.manifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should set the source properly", function() {
        assert.equal("http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp", this.actions[0].source);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });
    });

    describe("with a simple SQL layer", function() {
      before(function() {
        this.source = "file://./trailheads.sql";

        this.manifest = {
          layers: {
            trails: this.source
          }
        };

        this.actions = dd.getActions(this.manifest);
      });

      it("should use the psql driver", function() {
        assert.equal("psql", this.actions[0].driver);
      });

      it("should set the source properly", function() {
        assert.equal(this.source, this.actions[0].source);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });
    });

    describe("with an explicit SQL source", function() {
      before(function() {
        this.source = "file://./trailheads.sql";

        this.manifest = {
          layers: {
            trails: {
              source: this.source
            }
          }
        };

        this.actions = dd.getActions(this.manifest);
      });

      it("should use the psql driver", function() {
        assert.equal("psql", this.actions[0].driver);
      });

      it("should set the source properly", function() {
        assert.equal(this.source, this.actions[0].source);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });
    });

    describe("with a SQL command", function() {
      before(function() {
        this.sql = "CREATE EXTENSION postgis";

        this.manifest = {
          layers: {
            postgis: {
              sql: this.sql
            }
          }
        };

        this.actions = dd.getActions(this.manifest);
      });

      it("should use the psql driver", function() {
        assert.equal("psql", this.actions[0].driver);
      });

      it("should not have a source", function() {
        assert.ok(!this.actions[0].source);
      });

      it("should set 'sql'", function() {
        assert.equal(this.sql, this.actions[0].sql);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });
    });
  });

  describe("on subsequent runs", function() {
    describe("with changes to a simple Shapefile layer", function() {
      before(function() {
        this.oldSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp";
        this.newSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20131002.shp";

        this.oldManifest = {
          layers: {
            locations: this.oldSource
          }
        };

        this.newManifest = {
          layers: {
            locations: this.newSource
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.newSource, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("locations", this.actions[0].layer);
      });

      it("should set 'action' to 'update'", function() {
        assert.equal("update", this.actions[0].action);
      });
    });

    describe("with changes to a simple SQL layer", function() {
      before(function() {
        this.oldSource = "file://./trails.sql";
        this.newSource = "file://./trails2.sql";

        this.oldManifest = {
          layers: {
            trails: this.oldSource
          }
        };

        this.newManifest = {
          layers: {
            trails: this.newSource
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the psql driver", function() {
        assert.equal("psql", this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.newSource, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("trails", this.actions[0].layer);
      });

      it("should set 'action' to 'update'", function() {
        assert.equal("update", this.actions[0].action);
      });
    });

    describe("with changes to an explicit Shapefile layer", function() {
      before(function() {
        this.oldSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp";
        this.newSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20131002.shp";

        this.oldManifest = {
          layers: {
            locations: {
              source: this.oldSource,
              driver: "ogr"
            }
          }
        };

        this.newManifest = {
          layers: {
            locations: {
              source: this.newSource,
              driver: "ogr"
            }
          }
        };

        this.diff = jsonDiff(this.oldManifest, this.newManifest);

        this.actions = dd.getActions(this.diff, this.newManifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should update 'source'", function() {
        assert.equal(this.newSource, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("locations", this.actions[0].layer);
      });

      it("should set 'action' to 'update'", function() {
        assert.equal("update", this.actions[0].action);
      });
    });

    describe("with changes to an explicit SQL layer", function() {
      before(function() {
        this.oldSource = "file://./trails.sql";
        this.newSource = "file://./trails2.sql";

        this.oldManifest = {
          layers: {
            trails: {
              source: this.oldSource,
              driver: "psql"
            }
          }
        };

        this.newManifest = {
          layers: {
            trails: {
              source: this.newSource,
              driver: "psql"
            }
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the psql driver", function() {
        assert.equal("psql", this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.newSource, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("trails", this.actions[0].layer);
      });

      it("should set 'action' to 'update'", function() {
        assert.equal("update", this.actions[0].action);
      });
    });

    describe("with a change from a simple Shapefile layer to an explicit Shapefile layer", function() {
      before(function() {
        this.oldSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp";
        this.newSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20131002.shp";

        this.oldManifest = {
          layers: {
            locations: this.oldSource
          }
        };

        this.newManifest = {
          layers: {
            locations: {
              source: this.newSource,
              driver: "ogr"
            }
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.newSource, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("locations", this.actions[0].layer);
      });

      it("should set 'action' to 'update'", function() {
        assert.equal("update", this.actions[0].action);
      });
    });

    describe("with changes to an explicit SQL layer using a custom driver", function() {
      before(function() {
        this.oldSource = "file://./trails.sql";
        this.newSource = "file://./trails2.sql";
        this.driver = "custom";

        this.oldManifest = {
          layers: {
            trails: {
              source: this.oldSource,
              driver: this.driver
            }
          }
        };

        this.newManifest = {
          layers: {
            trails: {
              source: this.newSource,
              driver: this.driver
            }
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the custom driver", function() {
        assert.equal(this.driver, this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.newSource, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("trails", this.actions[0].layer);
      });

      it("should set 'action' to 'update'", function() {
        assert.equal("update", this.actions[0].action);
      });
    });

    describe("with changes to the driver used", function() {
      before(function() {
        this.source = "file://./trails.sql";
        this.oldDriver = "custom";
        this.newDriver = "psql";

        this.oldManifest = {
          layers: {
            trails: {
              source: this.source,
              driver: this.oldDriver
            }
          }
        };

        this.newManifest = {
          layers: {
            trails: {
              source: this.source,
              driver: this.newDriver
            }
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the new driver", function() {
        assert.equal(this.newDriver, this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.source, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("trails", this.actions[0].layer);
      });

      it("should set 'action' to 'update'", function() {
        assert.equal("update", this.actions[0].action);
      });
    });

    describe("with a layer removed", function() {
      before(function() {
        this.oldSource = "file://./trails.sql";

        this.oldManifest = {
          layers: {
            trails: this.oldSource
          }
        };

        this.newManifest = {
          layers: {}
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the psql driver", function() {
        assert.equal("psql", this.actions[0].driver);
      });

      it("should set 'layer'", function() {
        assert.equal("trails", this.actions[0].layer);
      });

      it("should set 'action' to 'remove'", function() {
        assert.equal("remove", this.actions[0].action);
      });
    });

    describe("with a layer added", function() {
      before(function() {
        this.trailSource = "file://./trails.sql";
        this.locationSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp";

        this.oldManifest = {
          layers: {
            trails: this.trailSource
          }
        };

        this.newManifest = {
          layers: {
            trails: this.trailSource,
            locations: this.locationSource
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should use the OGR driver", function() {
        assert.equal("ogr", this.actions[0].driver);
      });

      it("should set 'source'", function() {
        assert.equal(this.locationSource, this.actions[0].source);
      });

      it("should set 'layer'", function() {
        assert.equal("locations", this.actions[0].layer);
      });

      it("should set 'action' to 'create'", function() {
        assert.equal("create", this.actions[0].action);
      });
    });

    describe("with layers reordered", function() {
      before(function() {
        this.trailSource = "file://./trails.sql";
        this.locationSource = "http://data.stamen.com.s3.amazonaws.com/parks-conservancy/03_GGNPC_locations_20130417.zip/GGNPC_locations_20130417.shp";

        this.oldManifest = {
          layers: {
            locations: this.locationSource,
            trails: this.trailSource
          }
        };

        this.newManifest = {
          layers: {
            trails: this.trailSource,
            locations: this.locationSource
          }
        };

        this.actions = dd.getActions(this.oldManifest, this.newManifest);
      });

      it("should return an empty array", function() {
        assert.equal(0, this.actions.length);
      });
    });
  });
});
