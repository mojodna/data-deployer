"use strict";

var crypto = require("crypto");
var jsonDiff = require("jsondiffpatch").diff;

var detectDriver = function(source) {
  source = source || "";

  var driver;

  switch (true) {
  case !!source.match(/\.shp/):
    driver = "ogr";
    break;

  case !!source.match(/\.sql/):
    driver = "psql";
    break;

  default:
    throw new Error("Unrecognized layer type: " + source);
  }

  return driver;
};

var findChanges = function(diff, path) {
  diff = diff || {};
  path = path || [];

  if (Array.isArray(diff)) {
    return [{
      diff: diff,
      path: path
    }];
  }

  return Object.keys(diff).map(function(k) {
    return findChanges(diff[k], path.concat([k]));
  }).reduce(function(a, b) {
    return a.concat(b);
  }, []);
};

var generateVersion = function(layer) {
  var hash = crypto.createHash("md5");

  hash.update(JSON.stringify(layer), "utf8");

  return hash.digest("hex");
};

var getActionsForManifest = function(manifest) {
  var layers = manifest.layers;

  return Object.keys(layers).map(function(layer) {
    var driver,
        source,
        sql;

    var val = layers[layer],
        version = generateVersion(val);

    if (typeof(val) === "string") {
      source = val;
      driver = detectDriver(source);
    } else {
      if (val.source) {
        source = val.source;
        driver = val.driver || detectDriver(source);
      }

      if (val.sql) {
        sql = val.sql;
        driver = val.driver || "psql";
      }
    }

    return {
      driver: driver,
      layer: layer,
      source: source,
      sql: sql,
      version: version
    };
  });
};

var getActions = function(prev, manifest) {
  if (arguments.length === 1) {
    manifest = arguments[0];
    prev = {};
  }

  prev = prev || {};

  var actions = getActionsForManifest(manifest);

  if (Object.keys(prev).length === 0) {
    return actions.map(function(action) {
      action.action = "create";

      return action;
    });
  }

  var changes = findChanges(jsonDiff(prev, manifest));
  
  var layerChanges = changes.filter(function(x) {
    return x.path[0] === "layers";
  });

  return layerChanges.map(function(x) {
    var action,
        layer = x.path[1];

    if (x.diff.length === 3 &&
        x.diff[1] === 0 &&
        x.diff[2] === 0) {
      // layer deleted

      action = getActionsForManifest(prev)
        .filter(function(a) {
          return a.layer === layer;
        })[0];

      action.action = "remove";
    } else {
      action = getActionsForManifest(manifest)
        .filter(function(a) {
          return a.layer === layer;
        })[0];
        
      if (x.diff.length === 1) {
        action.action = "create";
      } else {
        action.action = "update";
      }
    }

    return action;
  });
};

module.exports = {
  detectDriver: detectDriver,
  findChanges: findChanges,
  getActions: getActions
};
